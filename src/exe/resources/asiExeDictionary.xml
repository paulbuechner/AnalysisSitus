<?xml version="1.0" encoding="Windows-1252"?>
<datadictionary version="1">

  <!-- ==================================================================== -->
  <!-- Common semantic IDs                                                  -->
  <!-- ==================================================================== -->

  <datum id="POI_X" format="%5.3x" label="x">
    <dimensions>
      <dimension name="extra" value="mm" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=0.01" />
    <description>X</description>
  </datum>

  <datum id="POI_Y" format="%5.3x" label="y">
    <dimensions>
      <dimension name="extra" value="mm" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=0.01" />
    <description>Y</description>
  </datum>

  <datum id="POI_Z" format="%5.3x" label="z">
    <dimensions>
      <dimension name="extra" value="mm" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=0.01" />
    <description>Z</description>
  </datum>

  <datum id="EdgeDiscrPrec" format="%5.5x" label="Discr. step">
    <domain type="Float" min="0" max="1e99" default="1"/>
    <ui widget="SpinBox" widgetParams="step=0.1"/>
    <description>Edge discretization step in model units.</description>
  </datum>

  <datum id="PrsOpacityCoeff" format="%5.5x" label="Opacity">
    <domain type="Float" min="0" max="1" default="1"/>
    <ui widget="SpinBox" widgetParams="step=0.1"/>
    <description>Opacity coefficient (as in VTK).</description>
  </datum>

  <datum id="PrsLineWidth" format="%5.5x" label="Line width">
    <domain type="Float" min="0" max="50" default="1"/>
    <ui widget="SpinBox" widgetParams="step=0.5"/>
    <description>Line width (as in VTK).</description>
  </datum>

  <datum id="PrsPointSize" format="%5.5x" label="Point size">
    <domain type="Float" min="0" max="50" default="5"/>
    <ui widget="SpinBox" widgetParams="step=0.5"/>
    <description>Point size (as in VTK).</description>
  </datum>

  <datum id="Fairing_coeff" format="%5.5x" label="Fairing coeff.">
    <domain type="Float" min="0" max="1e99" default="0.001"/>
    <ui widget="SpinBox" widgetParams="step=0.1"/>
    <description>Coefficient of fairness. Greater values correspond to smoother surfaces.
Make sure to pass non-zero values to surface approximation methods in order to avoid singular matrices.</description>
  </datum>

  <datum id="Num_iters" format="" label="Num. iters">
    <domain type="Integer" min="0" max="1e99" default="0"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Num. iters</description>
  </datum>

  <datum id="Num_USpans" format="" label="U spans">
    <domain type="Integer" min="1" max="1e99" default="1"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Number of knot spans in the U curvilinear direction of the initial plane (if surface name is not specified).</description>
  </datum>

  <datum id="Num_VSpans" format="" label="V spans">
    <domain type="Integer" min="1" max="1e99" default="1"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Number of knot spans in the V curvilinear direction of the initial plane (if surface name is not specified).</description>
  </datum>

  <datum id="Num_UIsos" format="" label="U isos">
    <domain type="Integer" min="1" max="1e99" default="1"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Number of intermediate isolines in the U curvilinear direction.</description>
  </datum>

  <datum id="Num_VIsos" format="" label="V isos">
    <domain type="Integer" min="1" max="1e99" default="1"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Number of intermediate isolines in the V curvilinear direction.</description>
  </datum>

  <datum id="Num_ProfilesS1" format="" label="Num. profiles (S1)">
    <domain type="Integer" min="1" max="1e99" default="5"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Number of intermediate isolines in the profile direction on the first surface.</description>
  </datum>

  <datum id="Num_ProfilesS2" format="" label="Num. profiles (S2)">
    <domain type="Integer" min="1" max="1e99" default="5"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Number of intermediate isolines in the profile direction on the second surface.</description>
  </datum>

  <datum id="Num_Guides" format="" label="Num. guides">
    <domain type="Integer" min="1" max="1e99" default="10"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>Number of intermediate guide curves on both surfaces.</description>
  </datum>

  <datum id="JoinSurfOffset" format="%5.5x" label="Boundary offset">
    <domain type="Float" min="0" max="1e99" default="1.0"/>
    <ui widget="SpinBox" widgetParams="step=0.5"/>
    <description>Boundary offset for smooth transitioning across the common edge.</description>
  </datum>

  <datum id="UDegree" format="" label="U degree">
    <domain type="Integer" min="1" max="8" default="3"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>U degree of the initial plane (if surface name is not specified).</description>
  </datum>

  <datum id="VDegree" format="" label="V degree">
    <domain type="Integer" min="1" max="8" default="3"/>
    <ui widget="SpinBox" widgetParams="step=1"/>
    <description>V degree of the initial plane (if surface name is not specified).</description>
  </datum>

  <valueList id="List-YesNo" name="Yes/No">
    <value id="1">Yes</value>
    <value id="2">No</value>
  </valueList>

  <valueList id="List-BVHBuilderAlgo" name="BVH algorithm">
    <value id="0">Binned</value>
    <value id="1">Linear</value>
  </valueList>

  <valueList id="List-ShapeDisplayMode" name="Display mode">
    <value id="0">None</value>
    <value id="1">Shading</value>
    <value id="4">Wireframe</value>
    <value id="32">Shading with edges</value>
  </valueList>

  <valueList id="List-IVShapeDisplayMode" name="Display mode">
    <value id="0">None</value>
    <value id="1">Shading</value>
    <value id="4">Wireframe</value>
    <value id="32">Shading with edges</value>
    <value id="64">Pure wireframe</value>
  </valueList>

  <valueList id="List-MeshDisplayMode" name="Display mode">
    <value id="1">Shading</value>
    <value id="2">Wireframe</value>
    <value id="4">Shrink</value>
  </valueList>

  <valueList id="List-SVOSamplingStrategy" name="Display mode">
	<value id="1">In</value>
	<value id="2">On</value>
	<value id="3">On | In</value>
	<value id="4">Out</value>
	<value id="5">In | Out</value>
	<value id="6">On | Out</value>
	<value id="7">In | On | Out</value>
  </valueList>

  <valueList id="List-ThicknessCheckType" name="Display mode">
    <value id="0">Ray-based</value>
    <value id="1">Sphere-based</value>
  </valueList>
	
	<!-- Sampling strategy for SVO voxels/points -->
  <datum id="Octree_SS" format="" label="Sampling strategy">
    <domain type="List" list="List-SVOSamplingStrategy" />
    <ui widget="ComboBox" />
    <description>Sampling strategy</description>
  </datum>

  <!-- Number of elements in SVO -->
  <datum id="Octree_NumElems" format="%5.2x3" label="Num. SVO nodes">
    <domain type="Integer" min="-1e99" max="1e99"/>
    <ui widget="Label" />
    <description>Num. SVO nodes</description>
  </datum>

  <!-- Read-only min scalar -->
  <datum id="MinScalarReadOnly" format="%5.2x3" label="Min. scalar">
    <domain type="Integer" min="-1e99" max="1e99"/>
    <ui widget="Label" />
    <description>Min. scalar</description>
  </datum>

  <!-- Read-only max scalar -->
  <datum id="MaxScalarReadOnly" format="%5.2x3" label="Max. scalar">
    <domain type="Integer" min="-1e99" max="1e99"/>
    <ui widget="Label" />
    <description>Max. scalar</description>
  </datum>

  <!-- BVH builder algorithm -->
  <datum id="BVHBuilderAlgo" format="" label="BVH algorithm">
    <domain type="List" list="List-BVHBuilderAlgo" />
    <ui widget="ComboBox" />
    <description>BVH algorithm</description>
  </datum>

  <!-- Display mode for shape presentation -->
  <datum id="PrsDisplayMode" format="" label="Display mode">
    <domain type="List" list="List-ShapeDisplayMode" />
    <ui widget="ComboBox" />
    <description>Display mode</description>
  </datum>

  <!-- Display mode for shape presentation -->
  <datum id="IVPrsDisplayMode" format="" label="Display mode">
    <domain type="List" list="List-IVShapeDisplayMode" />
    <ui widget="ComboBox" />
    <description>Display mode</description>
  </datum>

  <!-- Display mode for mesh presentation -->
  <datum id="PrsMeshDisplayMode" format="" label="Display mode">
    <domain type="List" list="List-MeshDisplayMode" />
    <ui widget="ComboBox" />
    <description>Display mode</description>
  </datum>

  <!-- Indicates whether the color for presentation is active -->
  <datum id="PrsHasCustomColor" format="" label="Has custom color">
    <domain type="Bool" />
    <ui widget="CheckBox" />
    <description>Custom color activation</description>
  </datum>

  <!-- Color for presentation -->
  <datum id="PrsCustomColor" format="" label="Color">
    <domain type="Integer" />
    <ui widget="ColorPicker" />
    <description>Color picker</description>
  </datum>

  <!-- Selector for directories -->
  <datum id="DirSelector" format="" label="">
    <domain type="String" />
    <ui widget="DirPath" widgetParams="title=Select directory..." />
    <description>Directory selector</description>
  </datum>

  <!-- Selector for files -->
  <datum id="FileSelector" format="" label="">
    <domain type="String" />
    <ui widget="OpenFilePath" widgetParams="title=Select file..." />
    <description>File selector</description>
  </datum>

  <datum id="Part_TX" format="%0.3g" label="X">
    <dimensions>
      <dimension name="extra" value="mm" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=0.01" />
    <description>X</description>
  </datum>

  <datum id="Part_TY" format="%0.3g" label="Y">
    <dimensions>
      <dimension name="extra" value="mm" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=0.01" />
    <description>Y</description>
  </datum>

  <datum id="Part_TZ" format="%0.3g" label="Z">
    <dimensions>
      <dimension name="extra" value="mm" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=0.01" />
    <description>Z</description>
  </datum>

  <datum id="Part_RX" format="%0.3g" label="RX">
    <dimensions>
      <dimension name="extra" value="deg" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=1." />
    <description>RX</description>
  </datum>

  <datum id="Part_RY" format="%0.3g" label="RY">
    <dimensions>
      <dimension name="extra" value="deg" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=1." />
    <description>RY</description>
  </datum>

  <datum id="Part_RZ" format="%0.3g" label="RZ">
    <dimensions>
      <dimension name="extra" value="deg" />
    </dimensions>
    <domain type="Float" min="-1e99" max="1e99" />
    <ui widget="SpinBox" widgetParams="step=1." />
    <description>RZ</description>
  </datum>

  <!-- Free text comment -->
  <datum id="FreeTextComment" format="" label="Comment">
    <domain type="String" />
    <ui widget="TextEditor" />
    <description>Free text comment</description>
  </datum>

  <!-- Thickness check types -->
  <datum id="ThicknessCheckType" format="" label="Type">
	<domain type="List" list="List-ThicknessCheckType" />
	<ui widget="ComboBox" />
    <description>Thickness check type</description>
  </datum>
	
  <!-- ==================================================================== -->
  <!-- DEFINITIONS OF DIMENSIONS AND ASSOCIATED UNITS                       -->
  <!-- ==================================================================== -->

  <dimension name="length">
    <unit name="m" si="yes"/>
    <unit name="mm" si="no" k="1e-3" b="0"/>
    <unit name="cm" si="no" k="1e-2" b="0"/>
    <unit name="km" si="no" k="1e3" b="0"/>
    <unit name="ft" si="no" k="0.3048" b="0"/>
    <unit name="in" si="no" k="0.0254" b="0"/>
  </dimension>
  <dimension name="mass">
    <unit name="g" si="no" k="1e-3" b="0"/>
    <unit name="kg" si="yes" />
    <unit name="Tonne" si="no" k="1e3" b="0"/>
    <unit name="Ton" si="no" k="907.18474" b="0"/>
    <unit name="Pound" si="no" k="0.45359237" b="0"/>
    <unit name="Kip" si="no" k="453.59237" b="0"/>
  </dimension>
  <dimension name="force">
    <unit name="N" si="yes"/>
    <unit name="kN" si="no" k="1e3" b="0"/>
  </dimension>
  <dimension name="time">
    <unit name="s" si="yes"/>
    <unit name="Minute" si="no" k="60" b="0"/>
    <unit name="Hour" si="no" k="3.6e3" b="0"/>
    <unit name="Day" si="no" k="86.4e3" b="0"/>
    <unit name="Month" si="no" k="2.6208e6" b="0"/>
    <unit name="Year" si="no" k="31.4496e6" b="0"/>
  </dimension>
  <dimension name="angle">
    <unit name="deg" si="no" k="0.01745329251994" b="0"/>
    <unit name="rad" si="yes"/>
  </dimension>
  <dimension name="temperature">
    <unit name="�K" si="yes"/>
    <unit name="�C" si="no" k="1" b="-273.15"/>
    <unit name="�F" si="no" k="0.5555555555556" b="-255.3722222222"/>
  </dimension>

</datadictionary>
