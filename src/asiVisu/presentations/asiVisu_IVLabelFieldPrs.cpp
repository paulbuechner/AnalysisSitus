//-----------------------------------------------------------------------------
// Created on: 05 February 2025
//-----------------------------------------------------------------------------
// Copyright (c) 2025-present, Sergey Slyadnev
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//    * Neither the name of the copyright holder(s) nor the
//      names of all contributors may be used to endorse or promote products
//      derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------

// Own include
#include <asiVisu_IVLabelFieldPrs.h>

// asiVisu includes
#include <asiVisu_IVLabelsDataProvider.h>
#include <asiVisu_LabelsPipeline.h>

// VTK includes
#include <vtkMapper.h>
#include <vtkProperty.h>
#include <vtkProperty2D.h>

//-----------------------------------------------------------------------------

asiVisu_IVLabelFieldPrs::asiVisu_IVLabelFieldPrs(const Handle(ActAPI_INode)& N)
: asiVisu_DefaultPrs(N)
{
  Handle(asiData_IVLabelFieldNode)
    node = Handle(asiData_IVLabelFieldNode)::DownCast(N);

  // Create data provider.
  Handle(asiVisu_IVLabelsDataProvider)
    DP = new asiVisu_IVLabelsDataProvider(node);

  // Create and register a pipeline.
  this->addPipeline        ( Pipeline_Main, new asiVisu_LabelsPipeline );
  this->assignDataProvider ( Pipeline_Main, DP );
}

//-----------------------------------------------------------------------------

Handle(asiVisu_Prs) asiVisu_IVLabelFieldPrs::Instance(const Handle(ActAPI_INode)& N)
{
  return new asiVisu_IVLabelFieldPrs(N);
}
