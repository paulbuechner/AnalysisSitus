<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: recognize cylindrical shafts</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

  <!-- [begin] Code highlight -->
  <link rel="stylesheet" href="../css/highlight.min.css">
  <script src="../js/highlight.min.js"></script>
  <!-- [end] Code highlight -->

  <!-- [begin] Pseudocode -->
  <script src="https://cdn.jsdelivr.net/npm/mathjax@2.7.9/MathJax.js?config=TeX-AMS_CHTML"
          integrity="sha256-DViIOMYdwlM/axqoGDPeUyf0urLoHMN4QACBKyB58Uw="
          crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            displayMath: [['$$','$$'], ['\\[','\\]']],
            processEscapes: true,
            processEnvironments: true,
        }
    });
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js"></script>
  <!-- [end] Pseudocode -->

</head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../features.html">features</a>/recognize cylindrical shafts
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='https://analysissitus.org/forum/index.php?resources/analysis-situs.6/'>Download</a></td>
  <td class="header-menu">
    <div class="dropdown">
      <button class="dropbtn">Features ...</button>
      <div class="dropdown-content">
        <a href="../features.html">Open source</a>
        <a href="../featuresext.html">Commercial</a>
      </div>
    </div>
  </td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</table></tr></td>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1>Recognize cylindrical shafts</h1>

<p>
A cylindrical shaft is a "hole inverted." Therefore, the algorithmic principles behind shafts recognition are similar to those of <a href="./features_recognize-drill-holes.html">drilled holes</a>. The following pseudocode describes the recognition algorithm.
</p>

<pre id="recognize-shafts" class="pseudocode">
  % Rule-based recognition of cylindrical shafts (www.analysissitus.org).
  \begin{algorithm}
  \caption{RecognizeShafts}
  \begin{algorithmic}
  \PROCEDURE{RecognizeShafts}{$S$} \Comment{$S$ is a CAD model.}
    \STATE $R$ $\gets \emptyset$ \Comment{$R$ is a result set of features.}
    \STATE visited $\gets \emptyset$
    \FOR{$f \in F$} \Comment{$F$ are all faces.}
      \IF{$f \notin$ visited}
        \STATE visited $\gets$ \CALL{ApplyRule}{$f$} \Comment{faces visited by the rule.}
      \ENDIF
    \ENDFOR
    \FOR{$c \subset G$} \Comment{$c$ is a connected component in the adjacency graph $G$.}
      \STATE $R \gets$ \CALL{PopulateFeature}{$c$} \Comment{Extract properties.}
    \ENDFOR
    \RETURN $R$
  \ENDPROCEDURE
  \end{algorithmic}
  \end{algorithm}
</pre>

<p>
Here $S$ is a CAD shape defined as a formal solid. The algorithm is a typical <a href="./features_recognition-principles.html#toc-feat-rec-rule-based">rule-based</a> template. The main body of the algorithm iterates the faces with a "cursor" (the Rule), which does all the useful job. Optionally, all the CAD faces detected as shaft elements can be labeled with the corresponding <a href="./features_aag.html">AAG</a> attribute.
</p>

<div align="center"><a href="../imgs/recognize-shafts_01.png"><img src="../imgs/recognize-shafts_01.png" width="60%"></a></div>

<p>
The following code snippet illustrates how to run the recognizer programmatically:
</p>

<pre><code>// Recognize shafts.
asiAlgo_RecognizeShafts recShafts(G);
//
if ( !recShafts.Perform(r) ) // `r` is the max radius.
{
  std::cout << "Shaft recognition failed." << std::endl;
  return 1; // Error.
}

// Get the extracted face indices.
const asiAlgo_Feature& resIndices = recShafts.GetResultIndices();

// Get the recognized shafts.
int idx = 0;
std::vector<Handle(asiAlgo_Shaft)> shafts = recShafts.GetShafts();
//
for ( const auto& shaft : shafts )
{
  std::cout << "Shaft "     << ++idx
            << " diameter " << shaft->diameter
            << " length "   << shaft->length
            << std::endl;
}
</code></pre>

<p>
  It should be emphasized that the recognizer not only returns the extracted face IDs, but it also groups shaft features according to topological connectivity and provides their properties, such as diameter and length.
</p>

<!-- Optional references to community resources -->
<div class="mg">
  <div class="mg-headline">Community reading:</div>
  <ul>
    <li class="fancy_ul_item">MG // <a href="https://quaoar.su/blog/page/recognition-of-shafts-has-been-open-sourced">Recognition of shafts has been open-sourced</a>.</li>
  </ul>
</div>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    <small>&copy; Quaoar&nbsp;Studio&nbsp;LLC 2015-present &nbsp; (Yerevan, Armenia)</small> | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

<script>
hljs.highlightAll();
pseudocode.renderClass("pseudocode");
</script>

</body>
</html>
