<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: about DFM</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

  <!--
    Quick navigation script. Use a div with "toc-panel" class having a
    nested div with "toc" id to place the navigation panel.
  -->
  <script src="../js/jquery-3.5.1.min.js"></script>
  <script src="../js/toc.js"></script>

  <!-- [begin] Code highlight -->
  <link rel="stylesheet" href="../css/highlight.min.css">
  <script src="../js/highlight.min.js"></script>
  <!-- [end] Code highlight -->

  <!-- [begin] Pseudocode -->
  <script src="https://cdn.jsdelivr.net/npm/mathjax@2.7.9/MathJax.js?config=TeX-AMS_CHTML"
          integrity="sha256-DViIOMYdwlM/axqoGDPeUyf0urLoHMN4QACBKyB58Uw="
          crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            displayMath: [['$$','$$'], ['\\[','\\]']],
            processEscapes: true,
            processEnvironments: true,
        }
    });
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js"></script>
  <!-- [end] Pseudocode -->

</head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../featuresext.html">extensions</a>/about DFM
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='https://analysissitus.org/forum/index.php?resources/analysis-situs.6/'>Download</a></td>
  <td class="header-menu">
    <div class="dropdown">
      <button class="dropbtn">Features ...</button>
      <div class="dropdown-content">
        <a href="../features.html">Open source</a>
        <a href="../featuresext.html">Commercial</a>
      </div>
    </div>
  </td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="dfm-about-toc">Design for Manufacturability (DfM) checks</h1>

<div class="toc-panel"><div id="toc"></div></div>

<h2 id="dfm-about-overview">Overview</h2>

<p>
  Analysis Situs provides a number of API services for analyzing manufacturability. Our NUMGRID conference talk [<a href="../references.html#slyadnev-dfm-2022">Slyadnev and Voevodin 2022</a>] gives a theoretical overview of the implemented tests. The DFM checks available in the scope of CNC recognition toolbox can be divided into three categories:
</p>

<ol>
  <li>Implicit: to be done by the client code based on the reported feature properties.</li>
  <li>Explicit inline: performed automatically and reported (if any) by default.</li>
  <li>Explicit optional: performed if the corresponding computation mode is enabled.</li>
</ol>

<p>
  Analysis Situs SDK would not report any issues based on the proportions, angles or absolute values of the reported feature properties. It is assumed that such "implicit" DFM rules have to be defined at the level of the client application, while Analysis Situs remains responsible for the extraction of geometric information about the part being inspected. The following example illustrates a slot, which is detected by Analysis Situs as an ordinary feature. It is left to the caller application to decide if this slot is "too narrow" or not, based on the <span class="code-inline">oppositeFaceDistance</span> property reported for this feature.
</p>

<div align="center"><a href="../imgs/ext/dfm-implicit_01.png"><img src="../imgs/ext/dfm-implicit_01.png" width="60%"/></a></div>

<p>
  By not reporting such "implicit" DFM issues, Analysis Situs remains flexible for configuration against a specific manufacturing shop. Deep narrow slots, deep holes, non-prismatic features, small-radius fillets, recommended groove distances and similar problems are therefore not reported directly and should be handled by the user application. At the same time, there are some "stronger" DFM issues, which are reported regardless of rules. A simplest example of such a "stronger" DFM issue is a <a href="./features_dfm-sharp-corners.html">sharp corner</a> that cannot be milled by traditional machining.
</p>

<div align="center"><a href="../imgs/ext/dfm-questions.png"><img src="../imgs/ext/dfm-questions.png" width="60%"/></a></div>

<div align="center"><a href="../imgs/ext/dfm-questions2.png"><img src="../imgs/ext/dfm-questions2.png" width="60%"/></a></div>

<p>
  Optional DFM checks are usually computationally heavier. They include such tests as the following ones:
</p>

<ol>
  <li><a href="./features_dfm-inaccessible-faces.html">Face accessibility analysis</a>.</li>
  <li><a href="./features_dfm-inaccessible-edges.html">Edge accessibility analysis</a>.</li>
  <li><a href="./features_dfm-thickness-analysis.html">Thickness analysis</a>.</li>
  <li><a href="./features_dfm-clearance-analysis.html">Clearance analysis</a>.</li>
</ol>

<p>
  All of the "heavy" inspections are done on a mesh representation of a part and require extensive ray casting. For the sake of overview, let us consider the benchmark test scenario illustrated in the following image:
</p>

<div align="center"><a href="../imgs/ext/dfm-benchmark_01.png"><img src="../imgs/ext/dfm-benchmark_01.png" width="60%"/></a></div>

<p>
  This part is almost entirely recognized, with the exception of a squared pocket that failed accessibility checks built into the contour recognizer. This is already an example of a "implicit" DFM check, which was done inline together with feature recognition logic. However, there are some more DFM issues reported in the JSON outcome:
</p>

<div align="center"><a href="../imgs/ext/dfm-benchmark_02.png"><img src="../imgs/ext/dfm-benchmark_02.png" width="60%"/></a></div>

<p>
  First come the "impossible" sharp corners:
</p>

<div align="center"><a href="../imgs/ext/dfm-benchmark_03.png"><img src="../imgs/ext/dfm-benchmark_03.png" width="60%"/></a></div>

<p>
  Then, a number of inaccessible faces are reported as distinct features. It is important to note that in this case, the approach directions were chosen parallel to the primary coordinate axes. This explains why angled holes were reported:
</p>

<div align="center"><a href="../imgs/ext/dfm-benchmark_04.png"><img src="../imgs/ext/dfm-benchmark_04.png" width="60%"/></a></div>

<p>
  All inaccessible faces are identified "as a whole." However, sometimes the CAD faces are simply too large to serve as a representative carrier of manufacturability issues. Alternatively, mesh elements representing the actual occluded portions of the CAD faces are given:
</p>

<div align="center"><a href="../imgs/ext/dfm-benchmark_05.png"><img src="../imgs/ext/dfm-benchmark_05.png" width="60%"/></a></div>

<p>
  Finally, inaccessible edges are also reported:
</p>

<div align="center"><a href="../imgs/ext/dfm-benchmark_06.png"><img src="../imgs/ext/dfm-benchmark_06.png" width="60%"/></a></div>

<h2 id="dfm-about-parameters">Parameters</h2>

<p>
  DFM is a part of CNC processing. The following table enumerates the argument keys to activate one or another DFM check.
</p>

<table align="center" style="border: 1px solid rgb(100, 100, 100);" cellpadding="5" cellspacing="0" width="100%">
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">-fx</span>
    </td>
    <td class="table-content-block">
      <p>
        Activates face accessibility check. This flag may optionally be followed with the <span class="code-inline">-dir</span> keyword enumerating all inspection directions. E.g., the following key combination activates accessibility tests in Z+, Y+ and X+ directions.
      </p>

      <pre><code>-fx -dir 0 0 1 0 1 0 1 0 0</code></pre>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">-ex</span>
    </td>
    <td class="table-content-block">
      <p>
        Activates edge accessibility check.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">-tx</span>
    </td>
    <td class="table-content-block">
      <p>
        Activates wall thickness check. This flag must be followed with the value of minimal allowed thickness.
      </p>

      <pre><code>-tx &lt;minThickness&gt;</code></pre>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">-cx</span>
    </td>
    <td class="table-content-block">
      <p>
        Activates clearance check. This flag must be followed with the value of minimal allowed clearance.
      </p>

      <pre><code>-cx &lt;minClearance&gt;</code></pre>
    </td>
  </tr>
</table>

<h1 id="dfm-executable">Executable</h1>

<p>
  Full CAD model inspection including CNC recognition and DFM checks can be launched via the <span class="code-inline">asiCncExe</span> executable with the following signature:
</p>

<pre>
&lt;input-filename&gt; &lt;output-filename&gt;
   [-nv]
   [-blendRadius <r>]
   [-holeRadius <r>]
   [-type {lathing|milling|default}]
   [-step] [-stepWithColors]                                          
   [-gltf] [-gltfWithColors]
   [-image]
   [-orient]
   [-fx]
   [-ex]
   [-tx &lt;minThickness&gt;]
   [-cx &lt;minClearance&gt;]
</pre>

<table align="center" style="border: 1px solid rgb(100, 100, 100);" cellpadding="5" cellspacing="0" width="100%">
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">&lt;input-filename&gt;</span>
    </td>
    <td class="table-content-block">
      <p>
        Input filename of a CAD design to inspect.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">&lt;output-filename&gt;</span>
    </td>
    <td class="table-content-block">
      <p>
        Output filename of a JSON file to generate.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-nv</span>
    </td>
    <td class="table-content-block">
      <p>
        Indicates whether to approximate negative volumes for the recognized features.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-blendRadius</span>
    </td>
    <td class="table-content-block">
      <p>
        The max allowed radius of a blend (fillet) feature.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-holeRadius</span>
    </td>
    <td class="table-content-block">
      <p>
        The max allowed radius of a hole (drill) feature.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-type</span>
    </td>
    <td class="table-content-block">
      <p>
        The enforced production type to recognize: milling, lathing or default (lathing + milling).
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-step</span><br>
      <span class="code-inline">-stepWithColors</span>
    </td>
    <td class="table-content-block">
      <p>
        Indicates whether to resave the processed part in STEP format (optionally, with colors). The following image illustrates the processed part saved as a STEP file with colors and imported in Catia V5:
      </p>
      <div align="center"><a href="../imgs/ext/cnc-colored-step.png"><img src="../imgs/ext/cnc-colored-step.png" width="60%"/></a></div>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-gltf</span><br>
      <span class="code-inline">-gltfWithColors</span>
    </td>
    <td class="table-content-block">
      <p>
        Indicates whether to export the processed part in glTF format (optionally, with colors). If one of those flags is specified, the detected DFM issues (if any) are also exported in a separate glTF file. The following image illustrates the recognized milled part loaded from a colored glTF file to a Three.js scene:
      </p>
      <div align="center"><a href="../imgs/ext/cnc-gltf_01.png"><img src="../imgs/ext/cnc-gltf_01.png" width="60%"/></a></div>
      <p>
        A separate glTF file with the detected manufacturability issues can be loaded into the same scene. This file provides a hierarchically arranged structure of all problems detected:
      </p>
      <div align="center"><a href="../imgs/ext/cnc-gltf_02.png"><img src="../imgs/ext/cnc-gltf_02.png" width="60%"/></a></div>
      <p>
        A glTF file with the manufacturability issues will only be generated if either <span class="code-inline">-gltf</span> or
        <span class="code-inline">-gltfWithColors</span> parameter is passed.
      </p>
      <div align="center"><a href="../imgs/ext/dfm-threejs.gif"><img src="../imgs/ext/dfm-threejs.gif" width="60%"/></a></div>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-image</span>
    </td>
    <td class="table-content-block">
      <p>
        Indicates whether to make a preview PNG image for the CAD design before processing. The filename of the preview image will be chosen automatically with the base name of <span class="code-inline">&lt;output-filename&gt;</span> and extension of <span class="code-inline">.png</span>.
      </p>
      <div align="center"><a href="../imgs/ext/cnc-preview-ex.png"><img src="../imgs/ext/cnc-preview-ex.png" width="40%"/></a></div>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-orient</span>
    </td>
    <td class="table-content-block">
      <p>
        Indicates whether to automatically reorient the part before feature recognition to better fit a hypothetical parallepiped stock.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-tx</span>
      <span class="code-inline">-cx</span>
      <span class="code-inline">-fx</span>
      <span class="code-inline">-ex</span>
    </td>
    <td class="table-content-block">
      <p>
        DFM checker switches.
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="260px">
      <span class="code-inline">-dir</span>
    </td>
    <td class="table-content-block">
      <p>
        The list of inspection directions to be passed to the face accessibility test.
      </p>
    </td>
  </tr>
</table>

<p>
  The executable prints all the passed parameters at the beginning of processing. This execution log can be shared with us if something goes wrong on inspection:
</p>

<pre class="code">
  ...      info: File to process:          '../NX-Mill-1.stp'.
  ...      info: Output JSON filename:     '../outcome.json'.
  ...      info: Max hole radius:           2e+100.
  ...      info: Max blend radius:          2e+100.
  ...      info: Dump STEP:                 true.
  ...      info: Dump glTF:                 true.
  ...      info: Dump image:                false.
  ...      info: Reorient:                  false.
  ...      info: Face accessibility:        true.
  ...      info: Edge accessibility:        true.
  ...      info: Thickness analysis:        true.
  ...      info: Min thickness:             2.
  ...      info: Clearance analysis:        true.
  ...      info: Min clearance:             2.
</pre>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    <small>&copy; Quaoar&nbsp;Studio&nbsp;LLC 2015-present &nbsp; (Yerevan, Armenia)</small> | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

<script>
hljs.highlightAll();
pseudocode.renderClass("pseudocode");
</script>

</body>
</html>
