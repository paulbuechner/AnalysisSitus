<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: CNC recognition workflow</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

  <!--
    Quick navigation script. Use a div with "toc-panel" class having a
    nested div with "toc" id to place the navigation panel.
  -->
  <script src="../js/jquery-3.5.1.min.js"></script>
  <script src="../js/toc.js"></script>

  <!-- [begin] Code highlight -->
  <link rel="stylesheet" href="../css/highlight.min.css">
  <script src="../js/highlight.min.js"></script>
  <!-- [end] Code highlight -->

  <!-- [begin] Pseudocode -->
  <script src="https://cdn.jsdelivr.net/npm/mathjax@2.7.9/MathJax.js?config=TeX-AMS_CHTML"
          integrity="sha256-DViIOMYdwlM/axqoGDPeUyf0urLoHMN4QACBKyB58Uw="
          crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            displayMath: [['$$','$$'], ['\\[','\\]']],
            processEscapes: true,
            processEnvironments: true,
        }
    });
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js"></script>
  <!-- [end] Pseudocode -->

</head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../featuresext.html">extensions</a>/CNC recognition workflow
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='https://analysissitus.org/forum/index.php?resources/analysis-situs.6/'>Download</a></td>
  <td class="header-menu">
    <div class="dropdown">
      <button class="dropbtn">Features ...</button>
      <div class="dropdown-content">
        <a href="../features.html">Open source</a>
        <a href="../featuresext.html">Commercial</a>
      </div>
    </div>
  </td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-cnc-workflow">CNC recognition workflow</h1>

<div class="toc-panel"><div id="toc"></div></div>

<p>
  CNC recognition aims at extracting machining properties from a part produced with classical subtractive techniques such as milling, turning, or turning plus milling. It begins with the "dumb" geometry in B-rep format, constructs the part's <a href="../features/features_aag.html">AAG</a>, performs a predefined workflow to determine the CNC manufacturing process (lathed, lathed + milled, milled), and then recognizes features with their properties.
</p>

<p>
  The following sections explain the primary steps in the CNC recognition workflow. Please keep in mind that CNC recognition is independent of sheet metal recognition, thus it is up to the client application to arrange these algorithms in one way or another. One viable technique is to run sheet metal recognition first, and then, if the input part remains unrecognized, finish the workflow with CNC recognition. Alternatively, both recognizers can be launched concurrently, with the desired production technique chosen a posteriori based on the recognition results (and possibly the resulting fabrication costs).
</p>

<h2 id="toc-cnc-check">Recognition commands</h2>

<p>
  The <span class="code-inline">cnc-check</span> command can be used in the <a href="../features/features_active-script.html">Active Script</a> session to perform CNC recognition for the active part. The same logic can be used from within C++ and <span class="code-inline">asiCncExe</span> executable. The following script illustrates a typical way of running the CNC recognition workflow:
</p>

<pre><code>> clear; load-part ex-lathing_05.stp
> cnc-check -nv</code></pre>

<div align="center"><a href="../imgs/ext/cnc-lathing_01-anim.gif"><img src="../imgs/ext/cnc-lathing_01-anim.gif" width="60%"/></a></div>

<p>
  If the <span class="code-inline">-nv</span> key is passed, the recognition procedure ends up constructing the negative volume cells for the turning features (if any). Among other parameters, the most frequently used are the <span class="code-inline">-blendRadius</span> and the <span class="code-inline">-holeRadius</span> to limit the sizes of the recognized fillets and drilled holes.
</p>

<p class="note">
  Negative volumes for milling features are not constructed explicitly but can be approximated based on other reported properties.
</p>

<div align="center"><a href="../imgs/ext/cnc-lathing_02.png"><img src="../imgs/ext/cnc-lathing_02.png" width="50%"/></a></div>
<div align="center"><a href="../imgs/ext/cnc-lathing_03.png"><img src="../imgs/ext/cnc-lathing_03.png" width="50%"/></a></div>
<div align="center"><a href="../imgs/ext/cnc-lathing_04.png"><img src="../imgs/ext/cnc-lathing_04.png" width="50%"/></a></div>

<p>
  CNC recognizer performs the following steps:
</p>

<ol>
  <li>Recognize the part as lathed.</li>
  <li>If lathed, subrecognize milling features.</li>
  <li>If not lathed, recognize the part as milled.</li>
</ol>

<p>
  It remains the intelligence of the algorithm to decide whether a part is lathed of milled. This decision is based on the identified rotational symmetries and the evaluation of the contribution of non-lathed features to the overall finished part. However, milling recognition can be enforced by supplying the <span class="code-inline">-type</span> keyword, for example:
</p>

<pre><code>> cnc-check -type milling</code></pre>

<p>
  The default colors associated with the recognized faces help to quickly grasp the recognized features. The milling features are normally green-ish, while the turned features are blue-ish or red-ish (depending on whether they are turned or bored).
</p>

<div align="center"><a href="../imgs/ext/cnc-milling_01.png"><img src="../imgs/ext/cnc-milling_01.png" width="50%"/></a></div>

<p>
  With the <span class="code-inline">-draw</span> option enabled, the <span class="code-inline">cnc-check</span> command renders a bunch of diagnostic visuals, such as pointwise depths and supplementary axes.
</p>

<div align="center"><a href="../imgs/ext/cnc-milling_02.png"><img src="../imgs/ext/cnc-milling_02.png" width="50%"/></a></div>

<p>
  The outcomes of CNC recognition are the JSON file with all extracted properties and optional artifact files, such as glTF meshes with submeshes for each CAD face. To generate filesystem artifacts, another command <span class="code-inline">cnc-perform</span> should be used, e.g.:
</p>

<pre><code>> cnc-perform ex-milling_02.stp ex-milling_02.json -gltfWithColors -image ex-milling_02.png</code></pre>

<p>
  This command is not interactive and is designed for batch processing. Together with the output JSON it can output the glTF meshes and a thumbnail image. The glTF format is open and can be used to communicate data between various systems, including web-viewers with Three.js API.
</p>

<div align="center"><a href="../imgs/ext/cnc-milling_03.png"><img src="../imgs/ext/cnc-milling_03.png" width="50%"/></a></div>

<p>
  Thumbnail images are useful for making preview of CAD files. These images are dumped in PNG format and do not require GPU for rendering, i.e. they can be generated in pure headless mode.
</p>

<div align="center"><a href="../imgs/ext/ex-milling_02.png"><img src="../imgs/ext/ex-milling_02.png" width="40%"/></a></div>

<h2 id="toc-steps">Recognition steps</h2>

<p>
  The following code snippet illustrates how to run CNC recognition from C++:
</p>

<pre><code>// Initialize settings.
const bool   computeVolumes = true;
const double maxHoleRadius  = 10; // mm
const double maxBlendRadius = 5; // mm
const bool   doReorient     = true;
const bool   allowSharp     = false;

// Prepare the inspection algorithm.
InspectFile inspector( shape,
                       inFilename,
                       progress,
                       plotter );
//
inspector.SetComputeVolumes      (computeVolumes);
inspector.SetMaxHoleRadius       (maxHoleRadius);
inspector.SetMaxBlendRadius      (maxBlendRadius);
inspector.SetAutoOrient          (doReorient);
inspector.SetSharpCornerContours (allowSharp);

// Perform inspection.
if ( !inspector.Perform() )
{
  progress.SendLogMessage(LogErr(Normal) << "Inspection failed.");
  return 1; // Error.
}

// Get the inspection results.
const Handle(InspectFileResult)& fileRes = inspector.GetResult();
// ...
</code></pre>

<p>
  The instance of <span class="code-inline">InspectFile</span> class is the main entry point to the recognition workflow. This tool implements the default scheme of recognition where each elementary algorithm has its own place with respect to others. For example, the default workflow assumes that the input part has to be reoriented optimally in the 3D space (although this mode is optional), and such reorientation is done in the very beginning (before AAG construction). The main processing stages are as follows:
</p>

<ol>
  <li>Optionally reorient the body.</li>
  <li>Build sparse visualization meshes for the body and compute summary information, such as AABB, surface area, volume, etc.</li>
  <li>Check if the body is lathed or milled and extract its features correspondingly.</li>
  <li>If the body is lathed, optionally build its max turned state (MTS) and the negative volume cells.</li>
</ol>

<p>
  The recognition results are programmatically accessible via <span class="code-inline">InspectFileResult</span> data structure, which is also serializable to JSON.
</p>

<!-- Optional references to community resources -->
<div class="mg">
  <div class="mg-headline">Community reading:</div>
  <ul>
    <li class="fancy_ul_item">MG // <a href="https://quaoar.su/blog/page/to-the-automatic-milling-feature-recognition">Open challenges in automatic feature recognition</a>.</li>
    <li class="fancy_ul_item">MG // <a href="https://quaoar.su/blog/page/interfacing-with-machine-learning-feature-recognition">Interfacing rule-based feature recognition with machine learning (AI) methods</a>.</li>
    <li class="fancy_ul_item">MG // <a href="https://quaoar.su/blog/page/assess-the-swept-volume-for-a-turned-part">Build a swept volume for a turned part</a>.</li>
  </ul>
</div>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    <small>&copy; Quaoar&nbsp;Studio&nbsp;LLC 2015-present &nbsp; (Yerevan, Armenia)</small> | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

<script>
hljs.highlightAll();
pseudocode.renderClass("pseudocode");
</script>

</body>
</html>
